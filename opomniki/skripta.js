window.addEventListener('load', function() {
	//stran nalozena
	//prijavain
	var obdelajKlik = function(){
		var ime = document.querySelector('#uporabnisko_ime').value;
		document.querySelector('#uporabnik').innerHTML = ime;
		var pokrivalo = document.querySelector('.pokrivalo');
		pokrivalo.style.visibility = "hidden";
	}
	var gumb = document.querySelector('#prijavniGumb');
	gumb.addEventListener('click', obdelajKlik);
	
	//dodajanaje opomnikov
	var dodaj = function(){
		var nazivOpomnika = document.querySelector('#naziv_opomnika').value;
		var casOpomnika = document.querySelector('#cas_opomnika').value;
		document.querySelector('#naziv_opomnika').value = "";
		document.querySelector('#cas_opomnika').value = "";
		document.querySelector('#opomniki').innerHTML += "\
			<div class='opomnik rob senc'> \
       			<div class='naziv_opomnika'>" +nazivOpomnika+ "</div> \
        		<div class='cas_opomnika'> Opomnik čez <span>" +casOpomnika+ "\
        		</span> sekund.</div> \
			</div>"		
	}
	var gumbDodaj = document.querySelector('#dodajGumb');
	gumbDodaj.addEventListener('click', dodaj);
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			if(cas == 0){
				var nazivOpomnika = document.querySelector('.naziv_opomnika').innerHTML;
				alert("Opomnik!\n\nZadolžitev '" + nazivOpomnika + "' je potekla!");
				document.querySelector('#opomniki').removeChild(opomnik);
			}
			else{
				casovnik.innerHTML = cas - 1;
			}
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

});

